#!/usr/bin/env python3
# encoding: utf-8
"""
shifty -- create a reduced-size copy of a FLAC / mp3 collection

shifty is a command-line tool to create a "format-shifted" copy of a large collection of audio files.

Its intended purpose is to use the lower quality mp3's on a portable audio player which may 
have a limited amount of disk space or is not capable of playing the original files. 

shifty recreates the original folder structure and encodes all FLAC files as mp3 files. It tries 
to copy audio tags supported by the id3v2 standard, including album covers if a suitable file 
is present in the source directory.

All other files are copied verbatim to the target directory.

Requires flac, metaflac, lame and eyeD3.


@author:     Daniel James

@copyright:  2015 Daniel James. All rights reserved.

@license:    GNU General Public License, Version 3 or later

@contact:    dj (at) slashspace (dot) org

@deffield    updated: 2015-08-01
"""

import logging
import multiprocessing
import os
import shlex
import subprocess
import sys

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter

__all__ = []
__version__ = 0.1
__date__ = "2015-08-01"
__updated__ = "2015-08-01"

DEBUG = 0

album_cover = ""
lame_preset = ""
overwrite = False
process_count = 1
sourcedir = ""
targetdir = ""

tag_synonyms = { "title": ["title"], "artist": ["artist"], "album": ["album"], "year": ["year", "release-year", "date"], "track": ["track", "tracknumber"], "genre": ["genre"] }

class CLIError(Exception):
    """Generic exception to raise fatal errors."""
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg
    
def flac_to_mp3(source, target):
    subprocess.check_call("flac -cd %s | lame --preset %s - %s" % (shlex.quote(source), lame_preset, shlex.quote(target)), stdout=subprocess.DEVNULL, shell=True)
    flac_tags = subprocess.check_output(["metaflac", "--export-tags-to=-", source], universal_newlines=True).split("\n")
    for flac_tag in flac_tags:
        try:
            tag_name, tag_value = flac_tag.split("=", 1)
        except ValueError as e:
            logging.debug(e)
            continue
            
        tag_name = tag_name.lower()
        for tag, synonyms in tag_synonyms.items():
            if tag_name in synonyms:
                if tag == "year":
                    # Only the year is supported by id3v2
                    tag_value = tag_value[0:4]
                subprocess.call(["eyeD3", "--no-color", "--%s=%s" % (tag, tag_value), target], stdout=subprocess.DEVNULL)
    
    if album_cover:
        cover_basedir = os.path.split(source)[0]
        cover_file = os.path.join(cover_basedir, album_cover)
        if os.path.exists(cover_file):
            subprocess.call(["eyeD3", "--no-color", "--add-image=%s:FRONT_COVER" % cover_file, target], stdout=subprocess.DEVNULL)
        
def ogg_to_mp3(source, target):
    logging.error("ogg to mp3 conversion not implemented yet\n")

def mp3_to_mp3(source, target):
    subprocess.check_call(["cp", source, target]) # TODO re-encode mp3s instead of just copying them

def any_to_any(source, target):
    subprocess.check_call(["cp", source, target])

def convert(file):
    target = file.replace(sourcedir, targetdir)    
    ext = file.rsplit(".", 1)[1].lower()
    convertFunc = None
    if ext == "flac":
        target = target.replace("flac", "mp3")
        convertFunc = flac_to_mp3
    elif ext == "ogg":
        convertFunc = ogg_to_mp3
    elif ext == "mp3":
        convertFunc = mp3_to_mp3
    else:
        convertFunc = any_to_any
    
    if os.path.exists(target) and not overwrite:
        logging.warn("Skipping %s. A file with that name already exists!" % target)
        return
    else:
        convertFunc(file, target)

def convert_single(file_list):
    for file in file_list:
        convert(file)

def convert_parallel(file_list, process_count):
    pool = None
    try:
        logging.debug("Creating the process pool")
        pool = multiprocessing.Pool(process_count)
        results = pool.map_async(convert, file_list)
        #Specify a timeout in order to receive control-c signal
        result = results.get(0x0FFFFF)
    finally:
        if pool:
            pool.terminate()
        logging.debug("All processes stopped")

def convert_files(file_list, process_count):
    if process_count < 2:
        convert_single(file_list)
    else:
        convert_parallel(file_list, process_count)
        

def main(argv=None): # IGNORE:C0111
    """Command line options."""

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = "%%(prog)s %s (%s)" % (program_version, program_build_date)
    program_shortdesc = __import__("__main__").__doc__.split("\n")[1]
    program_license = """%s

  Created by Daniel James on %s.
  Copyright 2015 Daniel James. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

USAGE
""" % (program_shortdesc, str(__date__))

    try:
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        
        # Positional arguments
        parser.add_argument("sourcedir", default=os.getcwd(), help="the source directory")
        parser.add_argument("targetdir", help="the target directory")
        
        # Optional arguments
        parser.add_argument("-a", "--albumcover", default="cover.jpg", help="the name of the album / folder cover art (default: 'cover.jpg')")       
        parser.add_argument("-l", "--logginglevel", default="WARNING", help="the logging level (DEBUG, INFO, WARNING, ERROR, CRITICAL)")
        parser.add_argument("-o", "--overwrite", action="store_true", help="overwrite existing files")
        parser.add_argument("-p", "--processes", default=os.cpu_count(), help="the number of processes to use (default: number of CPUs detected)")
        parser.add_argument("-q", "--quality", default="medium", help="the mp3 quality setting as defined by lame's --preset option (default: 'medium')")
        parser.add_argument("-s", "--followlinks", action="store_true", help="follow filesystem links")
        parser.add_argument("-V", "--version", action="version", version=program_version_message, help="print the program version and exit")

        args = parser.parse_args()

        logging.basicConfig(format="%(levelname)s:%(message)s", level=args.logginglevel.strip())
        
        global sourcedir, targetdir, processes, overwrite, album_cover, lame_preset
        sourcedir = args.sourcedir
        targetdir = args.targetdir
        
        if not targetdir.endswith("/"):
            targetdir += "/"
         
        if not os.path.exists(sourcedir):
            raise CLIError("Directory does not exist %s" % sourcedir)
        if not os.path.exists(targetdir):
            raise CLIError("Directory does not exist %s" % targetdir)
        if sourcedir == targetdir:
            raise CLIError("Source directory and target directory are identical!")
        if targetdir.startswith(sourcedir):
            raise CLIError("Target directory must not be a sub-directory of the source directory!")
        
        process_count = int(args.processes)
        logging.debug("Setting process count to %d" % process_count)
        overwrite = args.overwrite
        logging.debug("Existing files will be overwritten!")
        album_cover = args.albumcover
        logging.debug("Album cover art file name: %s" % album_cover) 
        lame_preset = args.quality
        logging.debug("lame preset: %s" % lame_preset)
        
        # Traverse the music directory tree, create the target directories 
        # and collect all source files.
        source_files = []
        for root, dirs, files in os.walk(sourcedir, followlinks=args.followlinks):
            for dirname in dirs:
                source = os.path.join(root, dirname)
                target = source.replace(sourcedir, targetdir)
                if not os.path.exists(target):
                    os.mkdir(target)
                    
            for filename in files:
                source = os.path.join(root, filename)
                source_files.append(source)

        return convert_files(source_files, process_count)

    except KeyboardInterrupt:
        logging.debug("Termination signal caught")
        print()
        return 0
    except Exception as e:
        if DEBUG:
            raise(e)
        
        logging.error(e.msg + "\nfor help use --help")
        return 2

if __name__ == "__main__":
    if DEBUG:
        pass
        #sys.argv.append("-l DEBUG")
        #sys.argv.append("-o") 
        #sys.argv.append("-p 1")
        #sys.argv.append("/home/daniel/Desktop/shifty/")
        #sys.argv.append("/home/daniel/Desktop/shifty-out/")
    sys.exit(main())