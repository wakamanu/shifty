# shifty
create a reduced-size copy of a FLAC / mp3 collection

##About

shifty is a command-line tool to create a "format-shifted" copy of a large collection of audio files.

Its intended purpose is to use the lower quality mp3's on a portable audio player which may have a limited amount of disk space or is not capable of playing the original files. 

shifty recreates the original folder structure and encodes all FLAC files as mp3 files. It tries to copy audio tags supported by the id3v2 standard, including album covers if a suitable file is present in the source directory.

All other files are copied verbatim to the target directory.

##Usage

`shifty [options]... [sourcedir] [targetdir]`

##Options

* **-h**, **--help**  
Show help message and exit
* **-a ALBUMCOVER**, **--albumcover ALBUMCOVER**  
the name of the album / folder cover art (default: 'cover.jpg')
* **-l LOGGINGLEVEL**, **--logginglevel LOGGINGLEVEL**  
the logging level (DEBUG, INFO, WARNING, ERROR, CRITICAL)
* **-o**, **--overwrite**  
overwrite existing files
* **-p PROCESSES**, **--processes PROCESSES**  
the number of processes to use (default: number of CPUs / cores detected)
* **-q QUALITY**, **--quality QUALITY**  
the mp3 quality setting as defined by lame's --preset option (default: 'medium')
* **-s**, **--followlinks**
follow filesystem links
* **-V**, **--version**
print the program version and exit

##Arguments

* **sourcedir**  
the source directory

* **targetdir**  
the target directory

##Examples

Run shifty with `folder.jpg` as the album cover art filename:

`shifty -a folder.jpg /path/to/music/ ./out/`

Run shifty with a single process even if a multi-core CPU is detected:

`shifty -p 1 /path/to/music/ ./out/`

##Note

Album covers are not copied from FLAC metadata. If you want the target files to be tagged with album covers, add an image file to each album's source directory and specify the file name with the -a (or --albumcover) option.

##Dependencies

shifty requires flac, metaflac, lame and eyeD3.

##Author

Copyright 2015 Daniel James <dj (at) slashspace (dot) org>

##License

shifty is released under the GNU General Public License, version 3 or later (see LICENSE).